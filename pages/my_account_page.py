from base.selenium_base import SeleniumBase
from base.locators import MyAccountLocators


class MyAccountPage(SeleniumBase):
    def nav_block_is_visible(self):
        nav_block = self.is_visible(*MyAccountLocators.ACCOUNT_NAV)
        return nav_block
