from base.randomaizer import Randomizer


class InputDAata:
    INVALID_EMAIL = Randomizer.random_char(7)
    INVALID_PASS = Randomizer.random_char(7)
    VALID_EMAIL_NOT_REGISTER = Randomizer.random_char(7) + "@" + Randomizer.random_char(7) + ".com"
    VALID_EMAIL = "andrii.l@interactivated.me"
    VALID_PASS = "Test_test"


class ExpectedData:
    EXPECTED_EMAIL_ERROR_INVALID = "Please enter a valid email address (Ex: johndoe@domain.com)."
    EXPECTED_ERROR_IN_TOP_1 = "The account sign-in was incorrect or your account is disabled temporarily. Please wait and try again later."
    REQUIRED_FIELD_ERROR = "This is a required field."