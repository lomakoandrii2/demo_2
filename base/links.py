class Links:
    LOGIN_URL = "https://import.aut1.stage.interactivated.me/en/customer/account/login/"
    MY_ACCOUNT_PAGE_INDEX = "https://import.aut1.stage.interactivated.me/en/customer/account/index/"
    REGISTRATION_PAGE = "https://import.aut1.stage.interactivated.me/en/customer/account/create/"
    MY_ACCOUNT_PAGE = "https://import.aut1.stage.interactivated.me/en/customer/account/"