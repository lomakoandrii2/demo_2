import random
import string


class Randomizer:
    @staticmethod
    def random_char(char_num):
        return ''.join(random.choice(string.ascii_letters) for _ in range(char_num))
